# Sport Results Table #

Using HTML to create and layout into a table sport results.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open sport_results.html in Sublime and have a look at its structure.
2. Find some sport results, for example a football league table or a tennis event results table.
3. Look at the way the table of data is laid out, use HTML to present the data in your own sport results table.
4. If you need to make changes to the way the table looks, open the style.css file and make changes.
5. You can see your progress by typing in terminal 'open sport_results.html'.